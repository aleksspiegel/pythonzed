    # -*- coding: utf- 8 -*-
print(u'Привет мир!')
print('Снова мир!')
print(u'Hello world!')
print('And Hi!')
print(u'Мы так же може добавить \n экранированный отступ')
print(u'Или \' такой знак или \" кавычки')

print(u'Сколько всего животных?')
print(u'Куры', 25 + 30/6)
print(u'Гуси', 100-25*3%4)
print(u'Гуси', 100-25%3*4)
print(u'Яйца', 3+2+1-5+4%2-1/4+6)
print(u'Верно ли', 3+2<5-7)
print(u'Верно ли', 5<=7)
print(u'Верно ли', 5<=-7)
# print(u'Коррень', 27//2)
print(u'Степень', 5**3)

name = u'Zed'
age = 35
height = 188
weight = 80

print(u'My name is %s' % name)
print(u'I am %d old' % age)
print(u'My height and weight is %d and %d' % (height, weight))
print(u'I am %r old' % age)


